from flask import Flask, request, render_template
import numpy as np
import tensorflow as tf


app = Flask(__name__)


def get_prediction(material_properties):
    linear_model = tf.keras.models.load_model(r"models\linear_model")
    y_pred = linear_model.predict(material_properties)

    return f"Модуль упругости при растяжении {y_pred}"


@app.route('/')
def index():
    return "main"


@app.route('/predict/', methods=['post', 'get'])
def processing():
    message = ''
    if request.method == 'POST':
        material_properties = request.form.get('username')

        material_properties_parameters = material_properties.split(" ")
        material_properties = [float(param) for param in material_properties_parameters]
        material_properties = np.array([material_properties])

        message = get_prediction(material_properties)

    return render_template('login.html', message=message)


if __name__ == '__main__':
    app.run()
